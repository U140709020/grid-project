﻿
public struct PositionInGrid
{

    // This struct for sending grid positon to necessary places

    public int height;
    public int width;



    public PositionInGrid(int _height, int _width)
    {
        height = _height;
        width = _width;
    }


}
