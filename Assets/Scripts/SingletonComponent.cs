﻿using UnityEngine;

public abstract class SingletonComponent<T> : MonoBehaviour where T : MonoBehaviour
{


    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();

                if (_instance == null)
                {
                    GameObject _container = new GameObject("_" + typeof(T).ToString());
                    _instance = _container.AddComponent<T>();
                }
            }

            return _instance;
        }
    }


    public static bool IsManagerActive()
    {
        return _instance != null;
    }


}