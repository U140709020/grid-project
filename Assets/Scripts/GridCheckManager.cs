﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCheckManager : SingletonComponent<GridCheckManager>
{

    /*
    * In this script, there are two matrices. One of them is for GameObject pooling, other one is for checking objects isClicked conditons in a sufficient way
    * And also this script is Singleton, so you can access it in any script
    * You can change minLimitForInactivatingNeighbours value with 5 then when 5 or more neighbourhood occurs, they will be inactived
    */



    [Header("Minimum Neighbour Count for Inactivating GameObjects")]
    public int minLimitForInactivatingNeighbours = 3; 





    public List<List<GameObject>> gameObjectMatrix = new List<List<GameObject>>();
    public List<List<bool>> isClickedMatrix = new List<List<bool>>();







    public void Check(PositionInGrid position)
    {
        isClickedMatrix[position.height][position.width] = true;

        List<PositionInGrid> neighboursList = new List<PositionInGrid>();
        neighboursList.Add(position);


        for (int index = 0; index < neighboursList.Count; index++)
        {
            if (neighboursList[index].width - 1 >= 0)
            {
                if (isClickedMatrix[neighboursList[index].height][neighboursList[index].width - 1])
                {
                    PositionInGrid neighbour1 = new PositionInGrid(neighboursList[index].height, neighboursList[index].width - 1);

                    if (!neighboursList.Contains(neighbour1))
                        neighboursList.Add(neighbour1);

                }
            }

            if (neighboursList[index].width + 1 < GridBuilderManager.Instance.gridWidth)
            {
                if (isClickedMatrix[neighboursList[index].height][neighboursList[index].width + 1])
                {
                    PositionInGrid neighbour2 = new PositionInGrid(neighboursList[index].height, neighboursList[index].width + 1);

                    if (!neighboursList.Contains(neighbour2))
                        neighboursList.Add(neighbour2);
                }
            }

            if(neighboursList[index].height - 1 >= 0)
            {
                if (isClickedMatrix[neighboursList[index].height - 1][neighboursList[index].width])
                {
                    PositionInGrid neighbour3 = new PositionInGrid(neighboursList[index].height - 1, neighboursList[index].width);

                    if (!neighboursList.Contains(neighbour3))
                        neighboursList.Add(neighbour3);
                }
            }

            if(neighboursList[index].height + 1 < GridBuilderManager.Instance.gridHeight)
            {
                if (isClickedMatrix[neighboursList[index].height + 1][neighboursList[index].width])
                {
                    PositionInGrid neighbour4 = new PositionInGrid(neighboursList[index].height + 1, neighboursList[index].width);

                    if (!neighboursList.Contains(neighbour4))
                        neighboursList.Add(neighbour4);
                }
            }

        }



        if (neighboursList.Count >= minLimitForInactivatingNeighbours)
        {
            for(int index = 0; index < neighboursList.Count; index++)
            {
                gameObjectMatrix[neighboursList[index].height][neighboursList[index].width].SetActive(false);
                isClickedMatrix[neighboursList[index].height][neighboursList[index].width] = false;
            }
        }



        neighboursList.Clear();
    }



}
