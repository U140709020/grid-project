﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GridBuilderManager : SingletonComponent<GridBuilderManager>
{


    /*
     * This script controles camera settings according the grid panel size, 
     * Also it controles grid panel width, height and creates necessary matrices according to grid's width and height
     * You can easily shrink or expand the grid panel with additional codes, it's very open to improvements
     */





    [Header("Grid Panel Settings")]
    [SerializeField] Transform gridPanel;
    public int gridWidth = 5;
    public int gridHeight = 5;
    [SerializeField] float spaceBetweenGridObjectsX = .1f;
    [SerializeField] float spaceBetweenGridObjectsY = .1f;


    [Header("GameObject Settings")]
    [SerializeField] GameObject gameObjectPrefab;


    // Distances between two objects with space distance too
    float distanceX;
    float distanceY;


    [Header("Camera Settings")]
    [SerializeField] Camera currentCamera;
    [SerializeField] float extraZoomDistance = 1f;



    // For loading objects to reduce the cost
    Coroutine coroutine;




    private void Awake()
    {
        getSizeOfGridObjectAndCalculateDistanceWithSpacesBetweenNeighbours();
    }

    private void Start()
    {
        // I didn't add loading scene image because I wanted to show you how I load objects, but while they're loading we can keep waiting player in loading screen
        loadGameObjectsWithCoroutine(createGridPanelAndNecessaryMatrices()); 
    }








    // Sprite's pixel per unit size can be different sometimes so according sprite object size, Grid Panel will be created
    public void getSizeOfGridObjectAndCalculateDistanceWithSpacesBetweenNeighbours()
    {
        float gridObjectSize_X = gameObjectPrefab.GetComponent<SpriteRenderer>().bounds.size.x;
        float gridObjectSize_Y = gameObjectPrefab.GetComponent<SpriteRenderer>().bounds.size.y;


        distanceX = spaceBetweenGridObjectsX + gridObjectSize_X;
        distanceY = -spaceBetweenGridObjectsY - gridObjectSize_Y;
    }








    private void setCameraZoomAccordingToScreenWidthAndHeight()
    {
        float cameraZoomDistance = Mathf.Max(gridWidth,gridHeight) * Mathf.Max(distanceX, distanceY) * Screen.height / Screen.width * .5f + extraZoomDistance;
        currentCamera.orthographicSize = cameraZoomDistance;
    }

    private void setCameraPosition()
    {
        float x = (gridWidth - 1) * distanceX * .5f;
        float y = (gridHeight - 1) * distanceY * .5f;

        currentCamera.transform.position = new Vector3(x, y, -10f);
    }

    // After expanding or shrinking the grid, you have to call this method to refresh camera settings according screen width and height
    public void refreshCameraSettings()
    {
        setCameraPosition();
        setCameraZoomAccordingToScreenWidthAndHeight();
    }








    // This script load game objects with per frame instead of suddenly
    public void loadGameObjectsWithCoroutine(IEnumerator method)
    {
        coroutine = StartCoroutine(method);
    }


    // In this script, you can create grid panel with necessary matrices in GridChekManager script
    public IEnumerator createGridPanelAndNecessaryMatrices()
    {

        for (int height = 0; height < gridHeight; height ++)
        {
            GameObject rowTransform = new GameObject(height.ToString());
            rowTransform.transform.SetParent(gridPanel);

            GridCheckManager.Instance.gameObjectMatrix.Add(new List<GameObject>());
            GridCheckManager.Instance.isClickedMatrix.Add(new List<bool>());

            for (int width = 0; width < gridWidth; width++)
            {
                GameObject go = Instantiate(
                    gameObjectPrefab, 
                    new Vector2(distanceX * width, distanceY * height),
                    Quaternion.identity,
                    rowTransform.transform
                    );

                go.GetComponent<GridObject>().addGridPositionForMatrix(height, width);

                GridCheckManager.Instance.gameObjectMatrix[height].Add(go);
                GridCheckManager.Instance.isClickedMatrix[height].Add(false);

                yield return 0;
            }

        }

        refreshCameraSettings();

        StopCoroutine(coroutine);
    }



}
