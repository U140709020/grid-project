﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{

    [Header("Icon Image")]
    [SerializeField] private GameObject iconImage;


    // Position in the grid panel
    int height;
    int width;


    // If this object is inactivated before and activated again, icon will be SetActive(false)
    private void OnEnable()
    {
        iconImage.SetActive(false);
    }


    private void OnMouseDown()
    {
        if (iconImage.activeSelf)
            return;


        iconImage.SetActive(true);

        GridCheckManager.Instance.Check(new PositionInGrid(height, width));
    }





    public void addGridPositionForMatrix(int _height, int _width)
    {
        height = _height;
        width = _width;
    }



}
