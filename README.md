This is a Unity project. If you want to run this, you have to install Unity 2019.2

You can dynamically create a grid with squares. (3x3 - 5x5 - 4x16 - 44x123 anything you want ʕ•́ᴥ•̀ʔっ)

After that, when you click a square, it will mark with "X".

If 3 or more X has neighborhood, they will be destroy. (Except cross neighborhood)

For example:

![GridProject](GridProject.png)